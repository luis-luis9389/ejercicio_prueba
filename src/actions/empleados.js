import { types } from "../types/types";

export const guardarEmpleado = (emp) => {
  return async (dispatch) => {
    const empleado = {
      id: new Date().getTime(),
      foto: "",
      ...emp,
    };
    await dispatch(saveEmpleado(empleado));
  };
};

const saveEmpleado = (emp) => {
  return {
    type: types.addEmpleado,
    payload: emp,
  };
};

export const empleaSeleccionado = (selecc) => {
  return async (dispatch) => {
    await dispatch(seleccionado(selecc));
  };
};

const seleccionado = (selecc) => {
  return {
    type: types.seleccionarEmpleado,
    payload: selecc
  }
}

export const vaciarSeleccion = () => {
  return async (dispatch) => {
    await dispatch(vaciar());
  };
};

const vaciar = () => {
  return {
    type: types.vaciarEmpleado
  }
}

export const editarEmpleado = (id, em) => {
  return async (dispatch) => {
    const e = {
      foto: "",
      ...em,
    };
    await dispatch(editarEmp(id, e));
  };
};

const editarEmp = (id, em) => {
  return {
    type: types.editarEmpleado,
    payload: {
      id: id.id,
      ...em,
      empresa: id.empresa,
    },
  };
};

export const guardarFoto = (foto, empleado) => {
  return async (dispatch) => {
    const empleadoFoto = {
      ...empleado,
      foto: foto
    };
    await dispatch(fotoEmpleado(empleadoFoto));
  };
};

const fotoEmpleado = (empleadoFoto) => {
  return {
    type:types.agregarFoto,
    payload: empleadoFoto
  }
}

export const buscarEmpleado = (empleado) => {
  return async (dispatch) => {
    await dispatch(buscando(empleado));
  };
};

const buscando = (empleado) => {
  return {
    type:types.buscarEmpleado,
    payload: empleado
  }
}