import Empleados from "./pages/Empleados";
import "./css/index.css";
import { Provider } from "react-redux";
import { store } from "./store/store";

function App() {
  return (
    <Provider store={store} >
      <Empleados />
    </Provider>
  );
}

export default App;
