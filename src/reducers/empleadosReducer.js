import { types } from "../types/types";

const initialState = {
  empleado: [],
  seleccionado: null,
  busqueda: [],
};

export const empleadosReducer = (state = initialState, action) => {
  // console.log(action);
  switch (action.type) {
    case types.addEmpleado:
      return {
        ...state,
        empleado: [...state.empleado, action.payload],
      };

    case types.seleccionarEmpleado:
      return {
        ...state,
        seleccionado: action.payload,
      };

    case types.vaciarEmpleado:
      return {
        ...state,
        seleccionado: null,
      };

    case types.editarEmpleado:
      return {
        ...state,
        empleado: state.empleado.map(emple => emple.id === action.payload.id ? action.payload : emple),
        seleccionado: null
      }

    case types.agregarFoto:
      return {
        ...state,
        empleado: state.empleado.map(emple => emple.id === action.payload.id ? action.payload : emple),
        seleccionado: null
      }

    case types.buscarEmpleado:
      return {
        ...state,
        busqueda: state.empleado.map(emple => emple.nombre === action.payload ? action.payload : emple)
      }

    default:
      return state;
  }
};
