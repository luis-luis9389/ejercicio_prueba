import React from "react";
import TablaEmpleados from "../components/TablaEmpleados";

const Empleados = () => {
  return (
    <section>
      <TablaEmpleados />
    </section>
  );
};

export default Empleados;
