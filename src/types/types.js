export const types = {
  // empleados
  addEmpleado: 'add Empleado',
  seleccionarEmpleado: 'select Empleado',
  vaciarEmpleado: 'vaciar Empleado',
  editarEmpleado: 'editar Empleado',
  agregarFoto: 'agregar Foto',
  // buscarEmpleado: 'buscar empleado',
};