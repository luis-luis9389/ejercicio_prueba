import React, { useState } from "react";
import { Button, Table } from "react-bootstrap";
import reactDom from "react-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  empleaSeleccionado,
  vaciarSeleccion,
  guardarFoto,
} from "../actions/empleados";
import ModalEmpleados from "./ModalEmpleados";
import Webcam from "react-webcam";
import Base64Downloader from "react-base64-downloader";
import NoImage from "../images/noimage.png";

const TablaEmpleados = () => {
  const dispatch = useDispatch();
  const { empleado } = useSelector((state) => state.empleados);

  const videoConstraints = {
    width: 150,
    height: 100,
    facingMode: "user",
  };

  const formatter = new Intl.NumberFormat("es-MX", {
    style: "currency",
    currency: "MXN",
    minimumFractionDigits: 0,
  });

  const webcamRef = React.useRef(null);

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setstate(imageSrc);
  }, [webcamRef]);

  const [state, setstate] = useState({});
  const [show, setShow] = useState(false);
  const [change, setChange] = useState(false);
  const [open, setOpen] = useState(false);

  const data = state;

  const handleClose = () => {
    setShow(false);
    dispatch(vaciarSeleccion());
  };

  const handleShow = () => setShow(true);

  const editEmpl = (em) => {
    dispatch(empleaSeleccionado(em));
    handleShow(true);
  };

  const camera = () => {
    setOpen(!open);
    setstate({});
  };

  const reiniciarfoto = (empleado) => {
    capture();
    setOpen(false);
  };

  const saveFoto = (empleado) => {
    dispatch(empleaSeleccionado(empleado));
    dispatch(guardarFoto(data, empleado));
    setOpen(false);
  };

  return (
    <>
      <p style={{ textAlign: "center", margin: "10px 0", fontSize:' 30px '}}>
        Total de empleados: {empleado.length}
      </p>
      <p style={{ textAlign: "center", margin: "10px 0", fontSize:' 30px '}}>
        Salario en: <strong style={{ color: 'green' }} >{change === true ? "USD" : "MXN"}</strong>
      </p>
      <div style={{ textAlign: "center" }}>
        <button
          style={{ margin: "30px 0" }}
          className="btn btn-primary"
          onClick={() => setChange(!change)}
        >
          Cambiar salario(MXN - USD)
        </button>
      </div>

      <Table striped bordered hover variant="dark">
        <thead style={{ textAlign: "center" }}>
          <tr>
            <th>Nombre(s)</th>
            <th>Apellidos</th>
            <th>Salario</th>
            <th>Nombre Empresa</th>
            <th>Foto</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {empleado.length === 0 ? (
            <tr>
              <th>sin empleados</th>
            </tr>
          ) : (
            empleado.map((emp) => (
              <tr style={{ textAlign: "center" }} key={emp.id}>
                <td>{emp.nombre}</td>
                <td>{emp.apellidos}</td>
                <td
                  className={emp.salario < 10000 ? "menor" : "mayor"}
                  style={{ fontFamily: "monospace" }}
                >
                  {change === true
                    ? formatter.format(emp.salario / 21.5)
                    : formatter.format(emp.salario)}
                </td>
                <td>{emp.empresa}</td>
                <td>
                  {
                    <img
                      src={emp.foto === "" ? NoImage : emp.foto}
                      width="100"
                      height="100"
                      alt=""
                    />
                  }
                </td>
                <td>
                  <Button
                    className="btn-editar"
                    onClick={() => editEmpl(emp)}
                  >
                    Editar
                  </Button>
                  <Button className="btn-editar" onClick={camera}>
                    Tomar Foto
                  </Button>
                  <Button className="btn-editar" onClick={() => saveFoto(emp)}>
                    Guardar Foto
                  </Button>
                  {state.length > 0 ? (
                    <>
                      <Base64Downloader
                        className="btn btn-primary"
                        base64={emp.foto}
                        downloadName="imagen_empleado"
                      >
                        Descargar
                      </Base64Downloader>
                    </>
                  ) : null}
                </td>
              </tr>
            ))
          )}
        </tbody>
      </Table>
      <div className="btn-container">
        <Button className="btn-añadir" onClick={handleShow}>
          Añadir
        </Button>
      </div>
      {reactDom.createPortal(
        <ModalEmpleados show={show} handleClose={handleClose} />,
        document.getElementById("modal")
      )}

      {state !== {} ? (
        <img src={open === false ? null : `${data}`} alt="" />
      ) : null}

      {open === true ? (
        <>
          <Webcam
            style={{ width: "100%", margin: "30px 0" }}
            audio={false}
            height={200}
            ref={webcamRef}
            screenshotFormat="image/jpeg"
            width={150}
            mirrored="true"
            videoConstraints={videoConstraints}
          />
          <div style={{
            display: 'flex',
            justifyContent: 'center'
          }}>
            <button
              style={{ margin: '0 10px' }}
              className="btn btn-primary"
              onClick={() => reiniciarfoto()}
            >
              Capturar Foto
            </button>
            <button
              style={{ margin: '0 10px' }}
              className="btn btn-primary"
              onClick={() => setOpen(false)}
            >
              cancelar
            </button>
          </div>
        </>
      ) : null}
    </>
  );
};

export default TablaEmpleados;
