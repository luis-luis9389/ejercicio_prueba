import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { buscarEmpleado } from '../actions/empleados';

const BuscarEmpleado = () => {

  const dispatch = useDispatch();

  const {empleado} = useSelector(state => state.empleados);
  console.log(empleado);

const [state, setstate] = useState({
  buscar: ''
});

const { buscar } = state;

const handleChange = (e) => {
  setstate({
    ...state,
    [e.target.name]:e.target.value
  })
}
  console.log(state);


const onSubmit = (e) => {
  e.preventDefault();
  dispatch(buscarEmpleado(state))
}

// useEffect(() => {
//   dispatch(buscarEmpleado(state))
// }, [state])



  return (
    <Form onSubmit={onSubmit} >
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Buscar</Form.Label>
        <Form.Control onChange={handleChange} value={buscar} name="buscar" autoComplete="off" type="text" placeholder="Buscar..." />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
};

export default BuscarEmpleado;
