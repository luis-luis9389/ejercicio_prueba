import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { guardarEmpleado, editarEmpleado } from "../actions/empleados";

const ModalEmpleados = ({ show, handleClose }) => {
  const dispatch = useDispatch();

  const { seleccionado } = useSelector((state) => state.empleados);

  const [formValues, setFormValues] = useState({
    nombre: "",
    apellidos: "",
    salario: 0,
    empresa: "",
  });

  const { nombre, apellidos, salario, empresa } = formValues;

  const handleChange = (e) => {
    setFormValues({
      ...formValues,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (
      nombre.trim() === "" ||
      apellidos.trim() === "" ||
      salario <= 0
    ) {
      console.log("campos obligatorios");
      return;
    }

    setFormValues({
      nombre: "",
      apellidos: "",
      salario: 0,
      empresa: "",
    });
  };

  const editEmp = () => {
    if (seleccionado === null) {
      dispatch(guardarEmpleado(formValues));
    } else {
      if (seleccionado !== null) {
        dispatch(editarEmpleado(seleccionado, formValues));
      }
    }
    handleClose();
  };

  return (
    <Modal show={show} onHide={() => handleClose()}>
      <Modal.Header closeButton>
        <Modal.Title>Registrar Empleado</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Nombre(s)</Form.Label>
            <Form.Control
              value={nombre}
              name="nombre"
              onChange={handleChange}
              autoComplete="off"
              type="text"
              placeholder="Nombre(s)"
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Apellidos</Form.Label>
            <Form.Control
              value={apellidos}
              name="apellidos"
              onChange={handleChange}
              autoComplete="off"
              type="text"
              placeholder="Apellidos"
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Salario</Form.Label>
            <Form.Control
              value={salario}
              name="salario"
              onChange={handleChange}
              autoComplete="off"
              type="number"
              placeholder="Salario"
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Empresa</Form.Label>
            <Form.Control
              value={ seleccionado === null ? empresa : seleccionado.empresa }
              readOnly={ seleccionado !== null ? true : null }
              name="empresa"
              onChange={handleChange}
              autoComplete="off"
              type="text"
              placeholder="Empresa"
            />
          </Form.Group>

          <Button
            style={{ marginRight: "20px" }}
            variant="secondary"
            onClick={() => handleClose()}
          >
            Cancelar
          </Button>
          <Button onClick={() => editEmp()} variant="primary" type="submit">
            Guardar
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default ModalEmpleados;
